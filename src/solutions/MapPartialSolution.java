package solutions;

public class MapPartialSolution {
	/*Pornind de la lista de documente de indexat ce va fi disponibila in fisierul de intrare, se determina dimensiunea fiecarui document si se creaza cate un task de tip MAP pentru fiecare fragment de cate D octeti dintr-un document (cu exceptia ultimului fragment, care poate fi mai scurt). Un task de tip MAP va avea urmatoarele informatii:
		Numele documentului
		Offset-ul de inceput al fragmentului din document
		Dimensiunea fragmentului*/
	
	public String nameDocument;
	public int startOffset;
	public int endOffset;
	
	public MapPartialSolution(String doc , int start , int end){
		this.nameDocument = doc;
		this.startOffset = start;
		this.endOffset = end;
		
	}
	
	public String toString() {
		// ...
		String ceva = "Fisierul este: " + this.nameDocument + " inceputul " + this.startOffset + " Sfarsitul: " + this.endOffset ;
		return ceva;

	}
}
