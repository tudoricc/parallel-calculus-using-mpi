package solutions;

import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;

public class ReducePartialSolution {
	
		public String nameDocument ;
		public ConcurrentHashMap<String, Integer> occurences ;
		
		public ReducePartialSolution(String name ,ConcurrentHashMap<String, Integer> mapResult ){
			this.nameDocument = name;
			this.occurences = mapResult;
		}
		
		public String toString(){
			String ceva = "Nume Document: " + this.nameDocument + "\nTable de aparitii(coincidente):\n" + this.occurences.toString() + "\n";
			return ceva;
		}
		
}
