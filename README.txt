Nume:Vasile Tudor
Grupa:331CC

Tema 2 APD

Dificultate tema:medie-spre ridicata

Implementare:
	Am inceput totul implementand cele 3 workpool-uri:unul de map,unul de 
compare si unul de reduce[fiecare avand clasa lui proprie]
	In main am realizat citirile de la linia de comanda si din fisierul de 
output , memorand unele variable in clasa main avand keyword-ul static inainte
pentru ca vroiam sa le folosesc si atunci cand prelucram un task la fiecare 
worker.
	De asemenea am evitat sa folosesc clase ca ArrayList si HashMap in tema mea
deoarece puteau aparea probleme la accesarea  in threaduri,de aceea am folosit 
clase ca Vector{in locul arrayList-ului} si ConcurrentHashMap penmtru ca am 
gasit cateva articole pe internet cum ca acestea erau mai thread safe.
	Functia folosita la citirea din fisier este :
	-readFromInput();
	In main am creat si o metoda createMapPartialSolution care imi crea
taskurile de map conform specificatiilor din enunt[daca cumva ajungi la sfarsit
am grija sa aloc un fragment mai micut].Rezultatul acestei functii il foloseam 
ca sa initiez taskurile de map si sa il adaug la workpool-ul de MAP.
	Alocam un vector de workeri pentru taskurile de map,le dau start si dupa 
aceea le dau join.

	Clasa SpecialWorker
	Este clasa care reprezinta un worker[numele nu este prea inspirat dar la 
inceput aveam Worker din laborator si aveam conflict de nume si am continuat 
cu ideea mea pana la capat].
	Acest obiect SpecialWorker are si un constructor care imi initializeaza cele
3 variable de clasa[cele 3 workpool-uri].
	Metoda run() este cea in care se desfasoara cea mai mare parte a programului
meu.Intr-un while incep prin a lua taskuri de tip map din workpool-ul MAP si 
daca cumva acest task este null trec la urmatorul tip de task-uri[reduce] , daca
nu incep sa prelucrez acel task,apeland metoda: reduceMapPartialSolution(
MapPartialSolution ps) .
	Dupa ce iau un task de tip-ul reduce din workpool-ul respectiv ,verific daca 
este null sau nu.Daca nu este null incep sa prelucrez task-ul apeland metoda
processRedPartialSolution(ReducePartialSolution redps),iar daca este null incep
sa prelucrez taskurile de tip COMPARE>
	Dupa ce iau u ntask de Compare din workpool-ul sau daca este null ies din 
bucla de while,iar daca nu este atunci incep sa il prelucrez prin metoda
processCmpPartialSolution(ComparePartialSolution cmpps)

	reduceMapPartialSolution:
	Aceasta functie primeste ca argument o solutie partiala[task] de tipul map
si incepe sa o prelucreze.In aceasta functie pur si simplu verific daca 
fragmentul meu incepe sau se termina in mijlocul unui cuvant[ma folosesc de 
functia isDelimitator].Daca cumva se intampla sa inceapa in mijlocul cuvantului
ma uit in stanga lui si vad ce se intampla[este cuvant sau nu ,in functie de ce
gasesc inaintea acelui caracter].La sfarsitul fragmentului ma uit dupa el si 
citesc pana gasesc un delimitator.AM folosit clasa RandomAccessFile pentur
citire[metodele input si seek].La sfarsitul functiei creez taskuri de reduce pe
care le adaug in workpool-ul corespunzator pentru urmatoarele threaduri.

	processRedPartialSolution:
	Aceasta functie imi ia un task creat mai sus si il proceseaza si adauga 
cuvintele din hashtabel-ul task-ului la un hashtable mai mare ,declarat in main
Iau cuvant cu cuvant din hash si ma uit daca exista in hash-ul din main si daca
nu il adaug ,iar daca exista incrementez numarul de aparitii

	processCmpPartialSolution:
	Functia aceasta imi ia taskurile de tip compare create in main si imi 
calculeaza gradul de similitudine[dupa formula din enunt].In aceasta functie
pe mine ma intereseaza doar cuvintele care apar in ambele tabele,pentur ca cele
care nu exista in ambele nu vor modifica similitudinea.



